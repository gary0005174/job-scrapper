from bs4 import BeautifulSoup
import requests
import re
import pandas as pd
import csv 
import time

title_list = []
company_list = []
location_list = []
description_list = []
date_list = []

#url for changing the page
def getURL(page):
    url1 = "https://hk.jobsdb.com" 
    url2 = "/hk/search-jobs/"
    url3 = "junior-data-engineer/"#make sure to change your jobs keyword here
    url4 = str(page)

    full_url = url1 + url2 + url3 + url4
    print(full_url)

    response = requests.get(full_url)

    return response

#func for countinging the number of pages
def pageCounting(data, className):
    soup = BeautifulSoup(data.text,'lxml')
    titles = soup.find_all(class_=className)

    return len(titles)


#func for append all job titles into a list
def filterTitle(data, className):

    soup = BeautifulSoup(data.text,'lxml')
    titles = soup.find_all(class_=className)
    allTitles = [title.text for title in titles]
    return allTitles


#func for append all company names into a list
def filterCompany(data):

    soup = BeautifulSoup(data.text,'lxml')
    companys = soup.find("div", class_="FYwKg _36UVG_0").find_all("span", class_="FYwKg _2Bz3E C6ZIU_0 _6ufcS_0 _2DNlq_0 _29m7__0")
    allCompanys = [company.text for company in companys]
    return allCompanys


#func for append all locations into a list
def filterLocation(data):

    soup = BeautifulSoup(data.text,'lxml')
    locations = soup.find("div", class_="FYwKg _36UVG_0").find_all("span", class_="FYwKg _3MPd_ _2Bz3E And8z")
    allLocations = [location.text for location in locations if location.text !='Company Confidential']
    return allLocations


#func for append all descriptions into a list
def filterDescription(data):

    soup = BeautifulSoup(data.text,'lxml')
    descriptions = soup.find_all("ul",class_="FYwKg _302h6 d7v3r _2uGS9_0")
    allDescriptions = [description.text for description in descriptions]
    return allDescriptions

#func for append all dates into a list
def filterDate(data):

    soup = BeautifulSoup(data.text,'lxml')
    dates = soup.find_all("span",class_="FYwKg _2Bz3E C6ZIU_0 _1_nER_0 _3KSG8_0 _29m7__0")
    allDates = [date.text for date in dates if date.text !='Company Confidential']
    return allDates


#while-loop for every 30 items then turn to next page
itemsCount = 30
page = 1
titleClass = "FYwKg _2j8fZ_0 sIMFL_0 _1JtWu_0"

while (itemsCount <= 30 and itemsCount != 0):
    d = getURL(page)
    itemsCount = pageCounting(d, titleClass)
    if itemsCount != 0:
        title_list.extend(filterTitle(d, titleClass))
        company_list.extend(filterCompany(d))
        location_list.extend(filterLocation(d))
        description_list.extend(filterDescription(d))
        date_list.extend(filterDate(d))

    page += 1


#store all data from above into a dataframe
d = {"job_title":title_list, "company name":company_list, "location": location_list, "dates" : date_list, "description": description_list}
df = pd.DataFrame.from_dict(d, orient='columns')
print(df)

#save as a csv file to your local PC
df.to_csv(r'D:\project\Jobs\DF.csv') #make sure to change your file location path and filename here